//------------------------------------------------
//  TIE-02200 Ohjelmoinnin peruskurssi -mallikoodi
//  Copyright © 2017  opersk@tut.fi
//------------------------------------------------

#include "todo_rakenne.hh"
#include "apufunktiot.hh"
#include <fstream>
#include <string>
#include <iostream>
#include <vector>

using namespace std;


// ----------------------------------------------------------------------------
// lue_tehtavatiedosto
// -------------------
// Funktio lukee parametrina saamansa tietovirran avulla tehtävälistatiedoston
// ja lisää sieltä saamansa tiedot tehtavarakenteeseen.  Jos tiedostossa
// on virheellisiä rivejä, funktion on palautettava false.  Jos tiedosto on
// virheetön, paluuarvon oltava true.  Tiedoston lukemin tämän funktion
// avulla EI SAA TULOSTAA NÄYTÖLLE MITÄÄN, sillä virheilmoitusten tulostuksesta
// on huolehdittu pääohjelmassa.
//
// KOKO TIEDOSTON LUKEMINEN ON TOTEUTETTAVA REKURSION AVULLA.  Tämä funktio,
// eikä sen avuksi itse toteutetut apufunktiot saa sisältää yhtään silmukka-
// rakennetta.  Silmukkarakenteet ovat tässä yhteydessä työn hylkäämisperuste.
//
// Funktion paluuarvo ja parametrien tyypit ja määrä on säilytettävä.
// ET SIIS SAA MUUTTAA LUE_TEHTAVATIEDOSTO-FUNKTION RAJAPINTAA.
// ----------------------------------------------------------------------------
bool lue_tehtavatiedosto(ifstream& virta, Todo& tehtavarakenne) {

    // Tallennetaan getlinella virta -muuttujan tieto string -tyyppiseen rivi -muuttujaan.
    // Getline pitää myös huolen rekursiivisuudesta eli hyppää automaattisesti seuraavaan
    // riviin uudessa kutsussa.

    string rivi;

    // Rekursion lopetusehto: palautetaan true, kun rivejä ei ole enää käsiteltävänä.

    if (not getline(virta, rivi)) {

        return true;
    }

    // Jos rivi ei ole oikeanmuotoinen, palautetaan false.

    if (not tarkista_rivi(rivi)) {

        return false;

    } else {

        // Muussa tapauksessa otetaan riviltä kiireellisyysaste ja tehtävä sekä lisätään
        // tiedot tehtävärakenteeseen.

        int kiireellisyys = stoi(rivi.substr(0,1));
        string tehtava = rivi.substr(2);
        tehtavarakenne.lisaa_tehtava(kiireellisyys, tehtava);

        // Siirrytään eteenpäin rekursiossa.

        if (lue_tehtavatiedosto(virta, tehtavarakenne)) {

            // Aina kun seuraava askel palauttaa true, kaikki on kunnossa. Tällöin palau-
            // tetaan takaisin true. Mikäli lähdetiedosto on kunnossa, lopullinen palautus-
            // arvo todo.cpp:lle on true.

            return true;

        } else {

            // Jos jossain kohtaa rivi ei ole ollut oikeanlainen, palautetaan false niin
            // kauan, kunnes rekursiosta 'ollaan päästy pois', ja palautetaan lopuksi
            // false todo.cpp:lle.

            return false;

        }

    }

}
