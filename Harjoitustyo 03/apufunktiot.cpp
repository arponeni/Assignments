//------------------------------------------------
//  TIE-02200 Ohjelmoinnin peruskurssi -mallikoodi
//  Copyright © 2017  opersk@tut.fi
//------------------------------------------------

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

const int MIN_KIIREELLISYYS = 1;
const int MAX_KIIREELLISYYS = 5;

void virheilmoitus(const string& teksti) {
    cout << "Virhe: " << teksti << endl;
}


vector<string> paloittele_sanoiksi(const string& teksti) {
    istringstream virta(teksti);
    vector<string> tulos = { };

    string sana;
    while ( virta >> sana ) {
        tulos.push_back(sana);
    }

    return tulos;
}


bool muuta_string_intiksi(const string& mjono, int& tulos) {
    istringstream virta(mjono);

    int aputulos;

    virta >> aputulos;
    if ( not virta ) {
        return false;
    }

    virta >> ws;
    virta.get();

    if ( not virta.eof() ) {
        return false;

    } else {

        tulos = aputulos;

        return true;
    }
}


string yhdista_valilyonneilla(vector<string>::const_iterator it,
                              vector<string>::const_iterator loppu) {
    if ( it == loppu ) {
        return "";

    } else if ( it + 1 == loppu ) {
        return *it;

    } else {

        return *it + " " + yhdista_valilyonneilla(it + 1, loppu);
    }
}

/* Funktion tarkoituksena on tarkastaa lähdetiedoston rivin oikeellisuus.
 * Parametri: const string& rivi: lähdetiedoston rivi.
 * Paluuarvo: true, jos rivi on oikeanlainen ja false, jos ei.
 */

bool tarkista_rivi(const string& rivi) {

    // Alustetaan apumuuttujat.

    string::size_type puolipisteen_paikka = 0;
    int taso = 0;

    // Etsitään riviltä ensimmäisen puolipisteen paikka.

    puolipisteen_paikka = rivi.find(';');

    // Erotellaan rivistä kiireellisyys ja tehtävä.

    string kiireellisyys_str = rivi.substr(0, 1);
    string tehtava = rivi.substr(2);

    // Jos kiireellisyyttä ei voi muuttaa int -tyyppiseksi, palautetaan false.

    if (not muuta_string_intiksi(kiireellisyys_str, taso)) {

        return false;

    // Jos rivillä ei ole puolipistettä, palautetaan false.

    } else if (puolipisteen_paikka == string::npos) {

        return false;

    // Jos rivi on tyhjä tai se sisältää vain välilyöntejä, metodi palauttaa string::npos.
    // Tällöin palautetaan false.

    } else if (tehtava.find_first_not_of(' ') == string::npos) {

        return false;

    // Ja jos kiireellisyys ei ole minimi- ja maksimiarvojen välissä, palautetaan false.

    } else if (stoi(kiireellisyys_str) < MIN_KIIREELLISYYS
               or stoi(kiireellisyys_str) > MAX_KIIREELLISYYS) {

        return false;

    // Muuten palautetaan true.

    } else {

        return true;

    }

}
