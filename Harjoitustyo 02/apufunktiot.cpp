// Moduulin tarkoituksena on toiminnallisuuksien suorittaminen.

#include "apufunktiot.hh"

#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <algorithm>
#include <iomanip>

using namespace std;

// Luodaan apunimet kahdelle tietorakenteelle.

using iso_tietorakenne = map<string, map< string, vector<Tuotetiedot>>>;
using pieni_tietorakenne = map<string, vector<Tuotetiedot>>;

struct Tuotetiedot {
    string tuotenimi;
    string hinta;
};

/* Funktion tarkoituksena on tulostaa kauppaketjut aakkosjärjestyksessä.
 *
 * Parametrit: const iso_tietorakenne& kauppatiedot: sisältää lähdetiedoston kaikki
 *             tiedot kaupoista, myymälöistä ja tuotteista. Ks. tarkempi selitys main.cpp.
 */

void tulosta_kauppaketjut(const iso_tietorakenne& kauppatiedot) {

    // Lähdetään käymään 'isoa tietorakennetta' läpi. Map järjestää ketjut aakkos-
    // järjestykseen automaattisesti.

    iso_tietorakenne::const_iterator ketju_iter;
    ketju_iter = kauppatiedot.begin();

    // Niin kauan, kun ketjuja riittää, tulostetaan iteraattorin osoittama ketju (->first)

    while ( ketju_iter != kauppatiedot.end() ) {

        cout << ketju_iter->first << endl;
        ++ketju_iter;

    }
}

/* Funktio tulostaa käyttäjän syöttämän ketjun myymälät aakkosjärjestyksessä.
 *
 * Parametrit: const iso_tietorakenne& kauppatiedot: ks. tulosta_kauppatiedot -funktio.
 *
 *             const string& ketju: käyttäjän syöttämän ketjun nimi.
 */

void tulosta_ketjun_myymalat(const iso_tietorakenne& kauppatiedot, const string& ketju) {
    
    // Etsitään kauppatiedoista ketjua vastaavan iteraattorin paikka.

    iso_tietorakenne::const_iterator ketju_iter;
    ketju_iter = kauppatiedot.find(ketju);

    // Jos find -metodin paluuarvo on end(), ketjua ei löydy ja tulostetaan virheilmoitus.

    if (ketju_iter == kauppatiedot.end()) {

        cout << "Virhe: ketjua ei löytynyt." << endl;

    } else {

        // Muussa tapauksessa ketju löytyi, joten lähdetään käymään myymäliä läpi.
        // Myymälät sijaitsivat map -rakenteessa ketjun hyötykuormana (ketju_iter->second)

        pieni_tietorakenne::const_iterator myymala_iter;
        myymala_iter = ketju_iter->second.begin();

        // myymala_iter käy läpi map -rakennetta, joten tiettyä myymälää voidaan osoittaa
        // laittamalla myymala_iter->first.

        while ( myymala_iter != ketju_iter->second.end() ) {

            cout << myymala_iter->first << endl;
            ++myymala_iter;
        }
    }   
}

/* Funktio tulostaa kauppatiedoista halvimman tuotteen hinnan ja mistä sitä saa. Kenties
 * koko ohjelman monimutkaisin funktio, sillä kauppatiedot tulee käydä kaksi kertaa läpi,
 * ja apumuuttujia tarvitaan suhteessa muihin funktioihin paljon.
 *
 * Parametrit: const iso_tietorakenne& kauppatiedot: ks. tulosta_kauppaketjut -funktio.
 *
 *             const string& tuote: käyttäjän syöttämän tuotteen nimi.
 */

void tulosta_halvimman_tiedot(const iso_tietorakenne& kauppatiedot, const string& tuote) {

    // Apumuuttujan 'onko_tuotetta_olemassa' oletusarvo on false. Käytetään hyväksi
    // selvitettäessä löytyykö tuotetta ylipäätään mistään kaupasta.

    bool onko_tuotetta_olemassa = false;

    // Halvimman hinnan oletusarvo on double -tyypin maksimiarvo.

    double halvin_hinta = numeric_limits<double>::max();

    // Lähdetään käymään ketjuja kerrallaan läpi (map -rakennetta).

    iso_tietorakenne::const_iterator ketju_iter;
    ketju_iter = kauppatiedot.begin();

    while (ketju_iter != kauppatiedot.end()) {

        // Käydään ketjun jokainen myymälä läpi (map -rakennetta, johon päästään käsiksi
        // käsiksi ketju_iter->second:in avulla).

        pieni_tietorakenne::const_iterator myymala_iter;
        myymala_iter = ketju_iter->second.begin();

        while (myymala_iter != ketju_iter->second.end()) {

            // Lopuksi käydään läpi myymälän tuotetiedot, jotka sijaitsevat tietueina
            // tallennettuina vektoriin. myymala_iter->second osoittaa nyt vektoriin.

            vector<Tuotetiedot>::const_iterator tuote_iter;
            tuote_iter = myymala_iter->second.begin();

            while (tuote_iter != myymala_iter->second.end()) {

                // tuote_iter osoittaa nyt yhtä tietuetta, joka sisältää yhden tuotteen
                // nimen ja hinnan.

                if (tuote_iter->tuotenimi == tuote) {

                    // Jos löydettiin oikea tuote, tuotetta on olemassa ainakin jossain
                    // kaupassa, joten apumuuttujan arvoksi tulee true.

                    onko_tuotetta_olemassa = true;

                    if (tuote_iter->hinta != "loppu") {

                        // Jos hinta ei ole 'loppu', se voidaan muuntaa string -tyypistä
                        // doubleksi.

                        double kasiteltava_hinta = stod(tuote_iter->hinta);

                        if (kasiteltava_hinta <= halvin_hinta) {

                            // Ja jos käsiteltävä hinta on halvempi kuin aiempi halvin
                            // hinta, päivitetään halvimman hinnan arvo.

                            halvin_hinta = kasiteltava_hinta;

                        }

                    }

                }

                ++tuote_iter;
            }

            ++myymala_iter;
        }

        ++ketju_iter;
    }

    // Jos tuotteeseen ei päästy käsiksi edellisessä silmukassa, sen oletusarvo säilyy,
    // eli tuotetta ei ole olemassa. Tällöin tulostetaan ilmoitus ja palataan mainiin.

    if (onko_tuotetta_olemassa == false) {

        cout << "Tuote ei kuulu valikoimiin." << endl;
        return;

    // Samoin tavoin, jos halvin_hinta -apumuuttujan alustusarvo säilyy, tuote on loppu
    // kaikkialta.

    } else if (halvin_hinta == numeric_limits<double>::max()) {

        cout << "Tuote on tilapäisesti loppu kaikkialta." << endl;
        return;

    // Muussa tapauksessa halvin hinta on löytynyt. Nyt täytyy käydä kauppatiedot läpi
    // vielä kerran, jotta saadaan selville, missä kaupoissa tuotetta saa halvimmalla
    // hinnalla.

    } else {

        // Apumuuttujaan 'tulostettavat_tiedot' lisätään ketjun nimi ja myymälä, jos
        // sieltä saa tuotetta halvimmalla hinnalla.

        set<string> tulostettavat_tiedot;

        // Käydään jälleen jokainen ketju läpi...

        iso_tietorakenne::const_iterator ketju_iter;
        ketju_iter = kauppatiedot.begin();

        while (ketju_iter != kauppatiedot.end()) {

            // ... ja jokaista ketjua vastaava myymälä ...

            pieni_tietorakenne::const_iterator myymala_iter;
            myymala_iter = ketju_iter->second.begin();

            while (myymala_iter != ketju_iter->second.end()) {

                // ... sekä jokaista myymälää vastaavat tuotteet.

                vector<Tuotetiedot>::const_iterator tuote_iter;
                tuote_iter = myymala_iter->second.begin();

                while (tuote_iter != myymala_iter->second.end()) {

                    // Kertauksen vuoksi, tuote_iter siis osoittaa tietuetta, joka sisälsi
                    // tuotenimen ja hinnan. Jos tuotenimi täsmää alkuperäiseen käyttäjän
                    // syöttämän tuotteeseen, ja se ei ole loppu, jatketaan eteenpäin.

                    if (tuote_iter->tuotenimi == tuote and tuote_iter->hinta != "loppu") {

                        // Käsiteltävä hinta voidaan nyt muuttaa double -tyyppiseksi.

                        double kasiteltava_hinta = stod(tuote_iter->hinta);

                        // Ja jos käsiteltävä hinta on halvin hinta, lisätään ketju- ja
                        // myymälätiedot settiin.

                        if (halvin_hinta == kasiteltava_hinta) {

                            string tieto = ketju_iter->first + " " + myymala_iter->first;
                            tulostettavat_tiedot.insert(tieto);

                        }
                        
                    }

                    ++tuote_iter;
                }

                ++myymala_iter;
            }

            ++ketju_iter;
        }

        // Lopuksi tulostetaan halvin hinta ja muut tulostettavat tiedot.

        cout << setprecision(2) << fixed << halvin_hinta << " euroa" << endl;

        set<string>::const_iterator tiedot_iter;
        tiedot_iter = tulostettavat_tiedot.begin();

        while (tiedot_iter != tulostettavat_tiedot.end()) {

            cout << *tiedot_iter << endl;
            ++tiedot_iter;

        }

    }

}

/* Funktio tulostaa jonkin myymälän tuotevalikoiman aakkosjärjestyksessä.
 *
 * Parametrit: const iso_tietorakenne& kauppatiedot: ks. tulosta_kauppaketjut -funktio.
 *
 *             const string& ketju: käyttäjän syöttämän ketjun nimi.
 *
 *             const string& myymala: käyttäjän syöttämän myymälän nimi.
 */

void tulosta_jonkin_tuotevalikoima(const iso_tietorakenne& kauppatiedot,
                                   const string& ketju, const string& myymala) {

    // Apumuuttujaan 'tuotteet_hinnat' tallennetaan nimensä mukaisesti myymälän tuotteet
    // ja hinnat. Map tarvitaan, jotta tuotevalikoima voidaan tulostaa järjestyksessä.

    map<string, string> tuotteet_hinnat;

    // Etsitään kauppatiedoista haluttua ketjua vastaavan iteraattorin paikka.

    iso_tietorakenne::const_iterator ketju_iter;
    ketju_iter = kauppatiedot.find(ketju);

    if (ketju_iter == kauppatiedot.end()) {

        // Jos ketjua ei löydy, tulostetaan virheilmoitus ja palataan mainiin.

        cout << "Virhe: ketjua ei löytynyt." << endl;
        return;

    } else {

        // Etsitään myös myymälää vastaavan iteraattorin paikka.

        pieni_tietorakenne::const_iterator myymala_iter;
        myymala_iter = ketju_iter->second.find(myymala);

        if (myymala_iter == ketju_iter->second.end()) {

            // Jos myymälää ei löydy, tulostetaan virheilmoitus ja palataan mainiin.

            cout << "Virhe: myymälää ei löytynyt." << endl;
            return;

        } else {

            // Muussa tapauksessa haluttu myymälä löytyi, joten lähdetään käymään sen
            // tuotteita läpi.

            vector<Tuotetiedot>::const_iterator tuote_iter;
            tuote_iter = myymala_iter->second.begin();

            while (tuote_iter != myymala_iter->second.end()) {

                // Lisätään tuotteet_hinnat -mappiin tuotenimi ja hinta.

                tuotteet_hinnat.insert({tuote_iter->tuotenimi, tuote_iter->hinta});
                ++tuote_iter;

            }

        }

    }

    // Kun tuotetiedot on saatu tallennettua apumappiin, lähdetään käymään sen tietoja
    // kerrallaan läpi.

    map<string, string>::const_iterator tuote_hinta_iter;
    tuote_hinta_iter = tuotteet_hinnat.begin();

    while (tuote_hinta_iter != tuotteet_hinnat.end()) {

        if (tuote_hinta_iter->second == "loppu") {

            cout << tuote_hinta_iter->first << " loppu" << endl;

        // Jos hinta ei ollut 'loppu', hinta voidaan muuntaa double -tyyppiseksi ja
        // tulostaa halutulla tarkkuudella.

        } else {

            double hinta = stod(tuote_hinta_iter->second);
            cout << tuote_hinta_iter->first << " " << setprecision(2) << fixed <<
                    hinta << endl;
        }

        ++tuote_hinta_iter;

    }

}

/* Funktio tulostaa lähdetiedoston kaikkien tuotteiden nimet aakkosjärjestyksessä.
 *
 * Parametri: const set<string>& tuotenimet: set, joka sisältää tuotenimet.
 */

void tulosta_tuotenimet(const set<string>& tuotenimet) {

    // set järjestää string -tyyppiset alkiot automaattisesti aakkosjärjestykseen, joten
    // tuotenimet voidaan tulostaa alusta alkaen kerrallaan.

    set<string>::const_iterator tuote_iter;
    tuote_iter = tuotenimet.begin();

    while (tuote_iter != tuotenimet.end()) {

        cout << *tuote_iter << endl;
        ++tuote_iter;

    }

}
