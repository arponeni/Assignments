#ifndef APUFUNKTIOT_HH
#define APUFUNKTIOT_HH

#include <string>
#include <vector>
#include <map>
#include <set>

using namespace std;
using iso_tietorakenne = map<string, map< string, vector<struct Tuotetiedot>>>;

// Sisältää apufunktiot.cpp:n funktioiden esittelyt. Kaikki parametrit ovat const- viite-
// parametreja, sillä niitä ei haluta muuttaa tai tehdä uutta kopiota.

void tulosta_kauppaketjut(const iso_tietorakenne& kauppatiedot);
void tulosta_ketjun_myymalat(const iso_tietorakenne& kauppatiedot, const string& myymalat);
void tulosta_halvimman_tiedot(const iso_tietorakenne& kauppatiedot, const string& tuote);
void tulosta_tuotenimet(const set<string>& tuotenimet);
void tulosta_jonkin_tuotevalikoima(const iso_tietorakenne& kauppatiedot,
                                   const string& ketju, const string& myymala);

#endif // APUFUNKTIOT_HH
