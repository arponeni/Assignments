//------------------------------------------------
//  TIE-02200 Ohjelmoinnin peruskurssi -mallikoodi
//  Copyright Â© 2017  opersk@tut.fi
//------------------------------------------------

#include "splitter.hh"

#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>

using namespace std;


// unsigned int -tyypin avulla voidaan kÃ¤sitellÃ¤ arvoja,
// jotka ovat suurempia tai yhtÃ¤suuria kuin nolla (etumerkittÃ¶miÃ¤),
// eli siis luonnollisia lukuja (â??).
const unsigned int SPLITTER_VIRHE = 0;


Splitter::Splitter(const string& paloiteltava_merkkijono):
    paloiteltava_(paloiteltava_merkkijono), kentat_( {} ) {
}


void Splitter::aseta_paloiteltava_merkkijono(const string& merkkijono) {
    paloiteltava_ = merkkijono;
    kentat_ = {};  // kentat.clear();
}


unsigned int Splitter::paloittele(char erotinmerkki, bool ohita_tyhjat) {
    // TyhjennetÃ¤Ã¤n kentat_-vektori siltÃ¤ varalta, ettÃ¤ samaa oliota
    // on jo aiemmin kÃ¤ytetty jokin toisen merkkijonon paloitteluun,
    // minkÃ¤ seurauksena vektorissa on jotain vanhaa tietoa.
    kentat_.clear();

    // alkukohta pitÃ¤Ã¤ kirjaa siitÃ¤, missÃ¤ kohdassa merkkijonoa
    // on seuraava kenttÃ¤, jota ei vielÃ¤ ole siirretty kentat_-vektoriin.
    string::size_type alkukohta = 0;
    string kentan_teksti;

    while ( true ) {
        // loppukohtaan etsitÃ¤Ã¤n find-metodilla, missÃ¤ kohdassa
        // on ensimmÃ¤inen alkukohdan jÃ¤lkeen tuleva erotinmerkki.
        string::size_type loppukohta;

        loppukohta = paloiteltava_.find(erotinmerkki, alkukohta);

        // Jos alkukohdan perÃ¤stÃ¤ ei lÃ¶ydy erotinmerkkiÃ¤, merkkojonossa
        // on enÃ¤Ã¤ yksi kenttÃ¤ jÃ¤ljellÃ¤ (viimeinen kenttÃ¤, jonka perÃ¤ssÃ¤
        // ei ole erotinmerkkiÃ¤).  KÃ¤sitellÃ¤Ã¤n se erikseen silmukÃ¤n perÃ¤ssÃ¤.
        if ( loppukohta == string::npos ) {
            break;
        }

        // Otetaan talteen kentÃ¤n sisÃ¤ltÃ¶ indeksien
        // alkukohta ja loppukohta vÃ¤listÃ¤.
        kentan_teksti = paloiteltava_.substr(alkukohta, loppukohta - alkukohta);

        // TyhjiÃ¤ kenttiÃ¤ ei tallenneta, mikÃ¤li parametri
        // ohita_tyhjat on true.
        if ( not (ohita_tyhjat and kentan_teksti.empty()) ) {
            kentat_.push_back(kentan_teksti);
        }

        // SiirretÃ¤Ã¤n alkukohta seuraavan kÃ¤sittelemÃ¤ttÃ¶mÃ¤n
        // kentÃ¤n ensimmÃ¤iseen merkkiin.
        alkukohta = loppukohta + 1;
    }

    // Otetaan viimeisen erotinmerkin perÃ¤ssÃ¤ oleva teksti talteen.
    // koska se on rivin viimeinen kenttÃ¤.
    kentan_teksti = paloiteltava_.substr(alkukohta);

    if ( not (ohita_tyhjat and kentan_teksti.empty()) ) {
        kentat_.push_back(kentan_teksti);
    }

    return kentat_.size();
}


unsigned int Splitter::kenttien_lukumaara() const {
    // Paloittelua ei ole suoritettu, jos kentat_-vektori on tyhjÃ¤.
    if ( kentat_.size() == 0 ) {
        return SPLITTER_VIRHE;

    } else {

        return kentat_.size();
    }
}


string Splitter::hae_kentta(unsigned int kentan_numero) const {
    // Paloittelua ei ole suoritettu tai kentÃ¤n numero on liian suuri.
    if ( kentat_.empty() or kentan_numero >= kentat_.size() ) {
        throw out_of_range("virheellinen kentan_numero");
    }

    return kentat_.at(kentan_numero);
}
