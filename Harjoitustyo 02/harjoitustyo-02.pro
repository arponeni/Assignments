TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
SOURCES += main.cpp \
    splitter.cpp \
    apufunktiot.cpp

DISTFILES += \
    tuotetiedot.txt

HEADERS += \
    splitter.hh \
    apufunktiot.hh
