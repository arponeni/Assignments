#ifndef SPLITTER_HH
#define SPLITTER_HH

#include <string>
#include <vector>

using namespace std;

// Sisältää splitter.cpp:n luokan esittelyn. Splitter on mallikoodi, joten sitä ei
// käydä tarkemmin läpi. splitter.cpp sisältää valmiit kommentit.

class Splitter {
  public:
    Splitter(const string& paloiteltava_merkkijono = "");
    void aseta_paloiteltava_merkkijono(const string& merkkijono);
    unsigned int paloittele(char erotinmerkki, bool ohita_tyhjat = false);
    unsigned int kenttien_lukumaara() const;
    string hae_kentta(unsigned int kentan_numero) const;

  private:
    string paloiteltava_;
    vector<string> kentat_;
};

#endif // SPLITTER_HH
