/* Ohjelmoinnnin peruskurssi TIE-02200 K2017
 * Ilari Arponen / 245637 / ilari.arponen@student.tut.fi
 * Harjoitustyö-02: Ohjelma lukee lähdetiedostosta kauppatietoja, tallentaa ne sopivaan
 * tietorakenteeseen ja osaa suorittaa erilaisia toimenpiteitä tietoihin liittyen.
*/

// Käyttöliittymä käyttää splitter- ja apufunktiot -moduuleita.

#include "splitter.hh"
#include "apufunktiot.hh"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set> 

using namespace std;

// Ohjelman toteutuksen ydin on tallentaa tiedot alla ilmaistuun tietorakenteeseen, johon
// palataan pian.

using iso_tietorakenne = map<string, map< string, vector<struct Tuotetiedot>>>;

// Tuotetiedot eli tuotenimi ja hinta tallennetaan Tuotetiedot -tietueeseen.

struct Tuotetiedot {
    string tuotenimi;
    string hinta;
};

/* Funktion tarkoituksena on lukea lähdetiedosto, tallentaa tiedot tietorakenteisiin sekä
 * ilmaista, tapahtuiko lähdetiedoston lukemisessa virhe.
 *
 * Parametrit: iso_tietorakenne& kauppatiedot: ohjelman fundamentaali tietorakenne, joka
 *             sisältää kaiken lähdetiedoston tiedon. Tarkempi selitys itse funktiossa.
 *
 *             set<string>& tuotenimet: settiin tallennetaan kaikki lähdetiedostosta
 *             löytyvät tuotenimet, jotta "tuotenimet" -toiminnon suorittaminen onnistuisi
 *             mahdollisimman helposti.
 *
 *             string TIEDOSTON_NIMI: sananmukaisesti lähdetiedoston nimi.
 *
 *             Parametrit eivät ole const -tyyppisiä, sillä tarkoituksena on saada
 *             aluksi tyhjät tietorakenteet täysiksi tiedoista.
 *
 * Paluuarvo:  bool true/false sen mukaan onnistuiko tiedoston luku tavalla tai toisella.
 *             Paluuarvo ei erittele, oliko esim. lähdetiedoston riveissä vikaa vai
 *             onnistuiko tiedoston luku ylipäätään.
*/

bool lue_ja_tallenna_tiedosto(iso_tietorakenne& kauppatiedot, set<string>& tuotenimet,
        string TIEDOSTON_NIMI) {

    // Avataan lähdetiedosto.

    ifstream tiedosto_olio(TIEDOSTON_NIMI);

    // Jos tiedostoa ei saada avattua (esim. väärä nimi tai ei löydy), palautetaan false.

    if (not tiedosto_olio) {

        return false;

    // Muussa tapauksessa lähdetään tarkastelemaan lähdetiedostoa rivi kerrallaan.

    } else {

        string rivi;

        while (getline(tiedosto_olio, rivi)) {

           // Luodaan Splitterin avulla 'riviolio'.

           Splitter osat(rivi);

           // Jos riviä ei voi paloitella puolipisteillä neljään osaan, sen muoto on
           // virheellinen ja palautetaan false.

           if (osat.paloittele(';', true) != 4) {

               return false;

           }

           // Muussa tapauksessa rivi on kelvollinen ja se jakaa osiin.

           string ketju = osat.hae_kentta(0);
           string myymala = osat.hae_kentta(1);
           string tuote = osat.hae_kentta(2);
           string hinta = osat.hae_kentta(3);

           // Luodaan tuote_hinta -vektori, joka pitää sisällään Tuotetiedot -tietueet.
           // Lisäksi tallennetaan tuotteen nimi tuotenimet -settiin.

           vector<Tuotetiedot> tuote_hinta;
           Tuotetiedot tuotetiedot = { tuote, hinta };
           tuotenimet.insert(tuote);

           /* Lähdetään käymään kauppatiedot -tietorakennetta läpi. Rakenne koostui siis 
            * mapista, jonka avaimena on kauppaketju ja hyötykuorma myymälä-map. Tämän 
            * mapin avaimena on myymälän sijainti ja hyötykuorma vektori, joka sisältää
            * tietueessa tuotetiedot.
           */ 
           
           // Etsitään aluksi, löytyykö 'laajimmasta' mapista kauppaketjua. 
           
           iso_tietorakenne::iterator ketju_iter;
           ketju_iter = kauppatiedot.find(ketju);

           // Jos kauppaketjua ei löydy pitää kaikki tiedot lisätä siihen heti.
           
           if (ketju_iter == kauppatiedot.end()) {

               // Lisätään tuote_hinta -vektoriin tuotetiedot -tietue. Sen jälkeen
               // lisätään kauppatietoihin kaikki tiedot eikä tehdä enempää.
               
               tuote_hinta.push_back(tuotetiedot);
               kauppatiedot.insert({ ketju, { { myymala, tuote_hinta } } });

           } else {

               // Muussa tapauksessa ketju löytyi tietorakenteesta, joten lähdetään
               // tarkastelemaan, mitä myymäliä se sisältää.
               
               map< string, vector<Tuotetiedot> >::iterator myymala_iter;
               myymala_iter = ketju_iter->second.find(myymala);

               // Jos myymälää ei löydy ketjun hyötykuormasta (huom. ->second ilmaus),
               // täytyy lisätä se.
               
               if (myymala_iter == ketju_iter->second.end()) {

                   // Lisätään taas tuotetiedot sitä vastaavaan vektoriin. Sitten voidaan
                   // lisätä 'myymälämappiin' myymälä ja sitä vastaavat tuotetiedot.
                   
                   tuote_hinta.push_back(tuotetiedot);
                   ketju_iter->second.insert({ { myymala, tuote_hinta } });

               } else {
                   
                   // Viimeinen mahdollisuus on, että sekä ketju että myymälä löytyvät jo
                   // tietorakenteesta. Lähdetään siis käymään läpi myymälän tuotteita.

                   vector<Tuotetiedot>::iterator tuote_iter;
                   tuote_iter = myymala_iter->second.begin();
                   
                   // Luodaan apumuuttuja, jonka oletusarvona on, että tuotetta ei löydy
                   // vielä myymälästä.
                   
                   bool tuote_loytyy_jo = false;
                   
                   while (tuote_iter != myymala_iter->second.end()) {
                       
                       // Käydään jokainen myymälän tuote läpi

                       if (tuote_iter->tuotenimi == tuote) {
                           
                           // Jos vektorista tulee vastaan tietue, joka sisältää tuotteen,
                           // apumuuttuja saa arvon true ja tuotteen hinta päivittyy.

                           tuote_loytyy_jo = true;
                           tuote_iter->hinta = hinta;

                       }

                       ++tuote_iter;

                   }

                   // Kun kaikki myymälän tuottet on käyty läpi, katsotaan löytyikö
                   // tuotetta. Jos apumuuttujan arvo on edelleen false, myymälään
                   // lisätään uudet tuotetiedot.
                   
                   if (tuote_loytyy_jo == false) {

                       myymala_iter->second.push_back(tuotetiedot);

                   }

                }

            }

        }

        tiedosto_olio.close();

    }

    // Näin käydään koko lähdetiedosto läpi, ja jos virheitä ei ilmene, palautetaan true.
    
    return true;
}

// Ohjelman käyttöliittymä, joka alustaa tietorakenteet ja osallistuu myös käyttäjän 
// syötteiden tarkasteluun.

int main() {
    
    // Alustetaan tietorakenteet. Tarkempi selitys löytyy 'lue_tiedosto_ja_tallenna' -
    // funktiosta.
    
    vector<Tuotetiedot> tuote_hinta;
    iso_tietorakenne kauppatiedot;
    set<string> tuotenimet;
    string TIEDOSTON_NIMI = "tuotetiedot.txt";

    // Luetaan lähdetiedosto ja tallennetaan tiedot tietorakenteisiin. Paluuarvona true/
    // false, sen mukaan onnistuiko luku.
    
    bool onnistuiko = lue_ja_tallenna_tiedosto(kauppatiedot, tuotenimet, TIEDOSTON_NIMI);

    // Jos luku ei onnistunut, tulostetaan virheilmoitus ja ohjelman suoritus päättyy.
    
    if (not onnistuiko) {
        
        cout << "Virhe: syötetiedostoa ei saatu luettua." << endl;
        return 0;
        
    }

    // Muussa tapauksessa tiedot on luettu ja tallennettu ja voidaan siirtyä varsinaiseen
    // käyttöliittymään.
    
    while (true) {

        cout << "tuotehaku> ";
        string komento;
        getline(cin, komento);

        // Käytetään Splitteriä käyttäjän syöttämän komennon tarkastelussa, eli luodaan
        // 'osat-olio'.
        
        Splitter osat(komento);

        // Jos käyttäjä syöttää tyhjän komennon tai siitä löytyy yli kolme osaa, se on
        // virheellinen, ja siirrytään takaisin kysymään uutta komentoa.
        
        if (osat.paloittele(' ', true) == 0 or osat.paloittele(' ', true) > 3) {

            cout << "Virhe: vääränlainen komento." << endl;
            continue;

        }

        /* Muussa tapauksessa käyttäjä on ainakin syöttänyt komennoksi jotain. Ensimmäisen
         * osan on tällöin pakko olla haluttu operaatio. Luodaan lisäksi apumuuttuja 'lkm'
         * joka ilmaisee komentojen osien lukumäärän. Tietty operaatio sisältää tietyn
         * määrän tarkentavia argumentteja, joten näin voidaan hyvin tarkastella syötteen
         * oikeellisuus.
        */
        
        string operaatio = osat.hae_kentta(0);
        unsigned int lkm = osat.kenttien_lukumaara();

        // Jos käyttäjä syöttää 'lopeta' (eikä mitään muuta), ohjelman suoritus päättyy.
        
        if (operaatio == "lopeta" and lkm == 1) {

            break;

        // Myös kauppaketjut -toiminto vaatii vain yhden komennon.

        } else if (operaatio == "kauppaketjut" and lkm == 1) {

            tulosta_kauppaketjut(kauppatiedot);

        // Samoin tavoin jatketaan muutkin operaatiot.

        } else if (operaatio == "myymalat" and lkm == 2) {

            string ketju = osat.hae_kentta(1);
            tulosta_ketjun_myymalat(kauppatiedot, ketju);

        } else if (operaatio == "halvin" and lkm == 2) {

            string tuote = osat.hae_kentta(1);
            tulosta_halvimman_tiedot(kauppatiedot, tuote);

        } else if (operaatio == "tuotevalikoima" and lkm == 3) {

            string ketju = osat.hae_kentta(1);
            string myymala = osat.hae_kentta(2);
            tulosta_jonkin_tuotevalikoima(kauppatiedot, ketju, myymala);

        } else if (operaatio == "tuotenimet" and lkm == 1) {

            tulosta_tuotenimet(tuotenimet);

        } else {

            cout << "Virhe: väärä komento." << endl;
            continue;

        }



    }
}
