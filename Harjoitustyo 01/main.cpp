// Ohjelmoinnin peruskurssi TIE-02200 K2017
// Ilari Arponen, 245637, ilari.arponen@student.tut.fi
// Harjoitustyö 1: Ohjelma käsittelee kolmea pankkitiliä ja osaa tehdä tulostus-, asetus-,
// otto-, pano- sekä tilisiirto-operaatioita.

#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

// Pankkitili -luokan avulla voidaan käsitellä yksittäistä tiliä.

class Pankkitili {

    // Ohessa metodit, joiden avulla pankkitilioliota voidaan käsitellä. Jos oliolle ei
    // aseteta oletusarvoja, annetaan sille alla olevat private -muuttujien arvot.

    public:
        Pankkitili(string tilinumero = "FI00 0000 0000 0000 00", double saldo = 0.0,
                   int asiakasnumero = 123456);
        void tulosta_tiedot() const;
        void aseta_tili(const string& tilinumero, const double& saldo,
                        const int& asiakasnumero);
        bool lisaa_rahaa(const double& pano);
        bool ota_rahaa(const double& otto);
        bool tilisiirto(Pankkitili& kohdetili, const double& rahamaara);

    // tilinumero_ : pankkitilin tilinumero (string).
    // saldo_ : tilin saldo (double).
    // asiakasnumero_ : tilin haltijan asiakasnumero (int).
    private:
        string tilinumero_;
        double saldo_;
        int asiakasnumero_;
};

// Luokan rakentaja, jossa oliolle annetaan tämän ohjelman tapauksessa oletusarvot.
// Parametrit: tilinumero string -tyyppisenä.
//             saldo double -tyyppisenä.
//             asiakasnumero int -tyyppisenä.

Pankkitili::Pankkitili(string tilinumero, double saldo, int asiakasnumero):

    tilinumero_(tilinumero), saldo_(saldo), asiakasnumero_(asiakasnumero) {

}

// Metodin avulla voidaan tulostaa yhtä pankkitilioliota koskevat tiedot.

void Pankkitili::tulosta_tiedot() const {

    cout << "Tilinumero: " << tilinumero_ << endl;
    // Tulostetaan tilin saldo kahden desimaalin tarkkuudelle setprecisionin avulla.
    cout << "Saldo: "  << setprecision(2) << fixed << saldo_ << endl;
    cout << "Asiakasnumero: " << asiakasnumero_ << endl;
}

// Metodin avulla voidaan muuttaa luotujen olioiden private -muuttujien arvoja.
// Parametreina: tilinumero string -tyyppisenä.
//               saldo double -tyyppisenä.
//               asiakasnumero int -tyyppisenä.

void Pankkitili::aseta_tili(const string& tilinumero, const double& saldo,
                            const int& asiakasnumero) {

    tilinumero_= tilinumero;
    saldo_ = saldo;
    asiakasnumero_ = asiakasnumero;
}

// Metodilla voidaan lisätä rahaa tilille.
// Parametri: 'pano' eli kuinka paljon halutaan lisätä rahaa tilille double -tyyppisenä.
// Paluuarvo: true/false sen mukaan, onnistuiko pano.

bool Pankkitili::lisaa_rahaa(const double& pano) {

    // Jos pano on positiivinen, lisäys suoritetaan ja palautetaan true.
    if (pano >= 0.0) {

        saldo_ += pano;
        return true;

    // Jos annettu summa oli negatiivinen, ei tehdä muuta kuin palautetaan false.
    } else {

        return false;
    }
}

// Metodi suorittaa oton, eli vähentää tilin saldoa annetun määrän verran.
// Parametri: "otto" eli kuinka paljon tililtä halutaan ottaa rahaa.
// Paluuarvo: true/false sen mukaan, oliko tilillä tarpeeksi katetta.

bool Pankkitili::ota_rahaa(const double& otto) {

    // Jos tilillä on saldoa, suoritetaan otto ja palautetaan true.
    if (otto <= saldo_) {

        saldo_ -= otto;
        return true;

    // Muuten palautetaan false.
    } else {

        return false;
    }
}

// Metodi suorittaa tilisiirron kahden pankkitiliolion välillä.
// Parametreina: "kohdetili", eli pankkitiliolio, jonne rahaa halutaan siirtää.
//               "siirtomaara", eli rahasumma, mikä halutaan siirtää.
// Paluuarvo: true/false sen mukaan, onnistuiko siirto.

bool Pankkitili::tilisiirto(Pankkitili& kohdetili, const double& siirtomaara) {

    // Jos lähtötilin saldolla on siirtomäärän verran katetta, siirto suoritetaan ja
    // palautetaan false.
    if (siirtomaara <= saldo_) {

        saldo_ -= siirtomaara;
        kohdetili.saldo_ += siirtomaara;
        return true;

    // Muutoin palautetaan false.
    } else {

        return false;
    }
}



// ---------------------------------------------------------------------------------------



// Funktion avulla luodaan pankkitilioliot ja lisätään ne vektoriin.
// Paluuarvo: tilit -vektori, joka sisältää kaikki tilioliot. Alkioiden tyyppinä
// Pankkitili.

vector<Pankkitili> luo_tilit() {

    vector<Pankkitili> tilit;

    // Luodaan oliot ja annetaan mielivaltaiset alustusarvot.
    Pankkitili eka("FI60 0000 1111 2222 33", 0.0, 111111),
               toka("FI61 1111 2222 3333 44", 0.0, 222222),
               kolmas("FI62 2222 3333 4444 55", 0.0, 333333);
    
    // Lisätään oliot vektoriin ja palautetaan mainiin.
    tilit.push_back(eka);
    tilit.push_back(toka);
    tilit.push_back(kolmas);

    return tilit;
}

// Funktion avulla pilkotaan mainissa annettu komento osiin, lisätään vektoriin ja 
// palautetaan kyseinen vektori. Vektorin alkioiden tyyppinä string.
// Parametrina: annettu komento string -muodossa.
// Paluuarvo: 'osat' -vektori, jonka alkioina string -tyyppiset komennon osat.

vector<string> pilko_komento(string& komento) {

    // Kyseisten muuttujien avulla käydään läpi komentoa ja pilkotaan se osiin.
    string::size_type aloitus = 0;
    string::size_type lopetus;
    
    // Vektori, johon komennon osat talletetaan.
    vector<string> osat;

    while (true) {
        
        // Kun find -metodi palauttaa arvon 'string::npos', ollaan komento käyty läpi (eli
        // (komennosta ei löytynyt enää välejä).
        if (lopetus == string::npos) {
            break;
        
        } else {
            
            // Etsitään findin avulla ensimmäisen vastaantulevan välin paikka.
            lopetus = komento.find(' ', aloitus);
            // Erotellaan haluttu kenttä substr -metodilla.
            string kentta = komento.substr(aloitus, lopetus-aloitus);
            // Lisätään kenttä listaan.
            osat.push_back(kentta);
            // Ja aloitetaan seuraavan välimerkin etsintä edellisen kentän lopusta.
            aloitus = lopetus + 1;
        }
    }

    // Lopuksi palautetaan halutut osat.
    return osat;
}

// Ohjelma käyttää kyseistä funktiota virheilmoitusten tulostamiseen. Näkemykseni mukaan
// negatiivisen rahasumman syöttäminen on todennäköisintä, joten laitoin sen oletus-
// parametriksi.
// Parametrit: selite string muodossa, tarkempi kuvaus virheen laadusta.

void tulosta_virheilmoitus(const string& selite =
                           "et voi syöttää negatiivista määrää rahaa.") {

    cout << "Virhe: " << selite << endl;
}

// Kyseinen funktio tarkastelee annetun komennon oikeellisuuden.
// Parametrit: vektori 'komennon_osat', jonka alkioina string -muodossa sen osat.
// Paluuarvo: bool -tyyppinen 'tapahtuiko_virhe'. Saa arvon true, jos komennon osissa
// on vikaa.

bool tarkastele_komentoa(const vector<string>& komennon_osat) {

    bool tapahtuiko_virhe = true;

    // Ensiksi tarkastellaan tapausta, jossa komento koostuu yhdestä osasta.
    if (komennon_osat.size() == 1) {
        string toimenpide = komennon_osat.at(0);

        // Ainoa mahdollinen syöte voi tällöin olla 'loppu'
        if (toimenpide != "loppu") {

            // Jos syöte oli jotain muuta, tulostetaan virheilmoitus ja palautetaan true.
            tulosta_virheilmoitus("väärä komento.");
            return tapahtuiko_virhe;
        }
    }

    // Toinen mahdollinen komento koostuu kahdesta osasta. Ehto kelpuuttaa toki myös
    // 'laajemman' komennon.
    if (komennon_osat.size() > 1) {

        // Oletuksena on, että komennon toinen osa on muunnettavissa int -muotoon.
        int eka_numero = stoi(komennon_osat.at(1));

        // Jos numero ei ole 1, 2 tai 3, tulostetaan virheilmoitus ja palautetaan true.
        if (eka_numero < 1 or eka_numero > 3) {
            tulosta_virheilmoitus("mahdolliset tilit ovat 1, 2 ja 3.");
            return tapahtuiko_virhe;
        }
    }

    // Viimeinen silmukka huolehtii kolmesta komennon osasta (kelpuuttaa myös laajemman
    // komennon, mutta oletetaan, että näin ei tapahdu.)
    if (komennon_osat.size() > 2) {

        // Oletuksena lisäksi, että kolmas osa on muunnettavissa int -muotoon.
        int toka_numero = stoi(komennon_osat.at(2));

        // Jälleen kelpuutetaan vain numerot 1,  tai 3.
        if (toka_numero < 1 or toka_numero > 3) {
            tulosta_virheilmoitus("mahdolliset tilit ovat 1, 2 ja 3.");
            return tapahtuiko_virhe;
        }
    }

    // Jos silmukkoihin ei päästä käsiksi, virhettä ei oletusten nojalla synny ja
    // palautetaan false.
    tapahtuiko_virhe = false;
    return tapahtuiko_virhe;
}

// Funktio suorittaa tiliolion tietojen asetuksen.
// Parametrit: kohdetili, eli Pankkitili -luokan olio, jonka asetuksia muutetaan.
//             eka_indeksi: kohdetiliä vastaava indeksi sitä vastaavassa vektorissa: esim
//                          tiliä 1 vastaa indeksi 0 jne.

void aseta_tiedot(Pankkitili& kohdetili, const int& eka_indeksi) {

    // Alustetaan muuttujat string -muodossa.
    string tilinumero, saldo_tekstina, asiakasnumero_tekstina;
    
    cout << "Syötä tilinumero: ";
    getline(cin, tilinumero);
    
    // 'saldo' muutetaan double -tyyppiseksi stod -metodilla.
    cout << "Syötä saldo: ";
    getline(cin, saldo_tekstina);
    double saldo = stod(saldo_tekstina);

    // 'asiakasnumero' muutetaan int -tyyppiseksi stoi -metodilla.
    cout << "Syötä asiakasnumero: ";
    getline(cin, asiakasnumero_tekstina);
    int asiakasnumero = stoi(asiakasnumero_tekstina);

    // Asetetaan tili ja tulostetaan, että asetus onnistui.
    kohdetili.aseta_tili(tilinumero, saldo, asiakasnumero);

    cout << "Tilin " << eka_indeksi + 1 << " tiedot asetettu." << endl;
}

// Funktion avulla kysytään halutun komennon tarvitsemaa rahasummaa.
// Parametri: string 'selite': haluttua toimenpidettä varten tarvitaan tarkentava määrite.
//            Esim. 'rahasumma' tai 'siirtosumma'.
double kysy_summa(const string& selite) {

    string raha_tekstina;

    cout << "Syötä " << selite << ": ";
    getline(cin, raha_tekstina);
    double raha = stod(raha_tekstina);

    return raha;

}

// Funktio suorittaa pano -toiminnon.
// Parametrit: 'lahtotili', eli Pankkitili -luokan olio, jolle lisätään rahaa.
//           : eka_indeksi, eli kohdetili -oliota vastaava indeksi oliovektorissa.
void suorita_pano(Pankkitili& lahtotili, const int& eka_indeksi) {

    double rahamaara = kysy_summa("rahamäärä");

    // Metodi palauttaa true, jos pano onnistui.
    if (lahtotili.lisaa_rahaa(rahamaara)) {
        cout << "Pano suoritettu tilille " << eka_indeksi + 1 << "." << endl;

    // Muulloin tulostetaan virheilmoitus.
    } else {
        tulosta_virheilmoitus();
    }
}

// Funktio suorittaa otto -toiminnon.
// Parametrit: ks. ylempi 'suorita_pano' -funktio.
void suorita_otto(Pankkitili& lahtotili, const int& eka_indeksi) {

    double rahamaara = kysy_summa("rahamäärä");
    
    // Jos 'rahamaara' on positiivinen, jatketaan eteenpäin.
    if (rahamaara >= 0.0) {

        // Metodi palauttaa true, jos otto onnistui.
        if (lahtotili.ota_rahaa(rahamaara)) {
            cout << "Otto suoritettu tililtä " << eka_indeksi + 1 << "." <<
                    endl;

        // Jos metodi palauttaa false, tulostetaan oheinen virheilmoitus.
        } else {
            tulosta_virheilmoitus("tilillä ei ole tarpeeksi saldoa.");
        }

    // Jos 'rahamaara' oli negatiivinen tulostetaan oletusarvoinen virheilmoitus.
    } else {
        tulosta_virheilmoitus();
    }
}

// Funktio suorittaa tilisiirron kahden Pankkitili -luokan olion välillä.
// Parametrit: 'lahtotili': olio, jolta siirretään rahaa.
//             'kohdetili': olio, jonne siirretään rahaa.
void suorita_tilisiirto(Pankkitili& lahtotili, Pankkitili& kohdetili) {

    double rahamaara = kysy_summa("siirtomäärä");

    // Ulompi ehtorakenne tarkastelee rahasumman arvoa.
    if (rahamaara >= 0.0) {

        // Jos tilisiirto -metodi palauttaa falsen, siirto ei onnistunut. Jos siirto
        // onnistuu, ei tulosteta mitään.
        if (lahtotili.tilisiirto(kohdetili, rahamaara) == false) {
            tulosta_virheilmoitus("lähtötilillä ei ole tarpeeksi saldoa.");
        }

    } else {

        // 'rahamaaran' negatiivisuuden huomioiminen.
        tulosta_virheilmoitus();
    }
}



// -----------------------------------------------------------------------------------------



int main() {

    // Alustetaan syötettävä komento.
    string komento = "";

    // Ja 'tilit' -vektori, johon tilioliot tallennetaan.
    vector <Pankkitili> tilit;

    // Sekä 'komennon_osat' -vektori, johon osat sananmukaisesti komennon osat säilötään.
    vector <string> komennon_osat;

    // Luodaan tilioliot ja talletetaan ne kyseiseen listaan.
    tilit = luo_tilit();

    while (true) {

        cout << "pankkitilitesti> ";
        getline(cin, komento);

        // Pilkotaan komento osiksi.
        komennon_osat = pilko_komento(komento);
        string toimenpide = komennon_osat.at(0);

        // Tarkastellaan komennon oikeellisuutta.
        if (tarkastele_komentoa(komennon_osat)) {
            continue;

        } else if (toimenpide == "loppu") {
            break;

        // Jos läpäistään komentoa koskevat nyanssit, siirrytään itse toimenpiteisiin.
        } else {

            // Otetaan komentoa vastaava indeksi sekä sitä vastaava tiliolio käyttöön.
            int eka_indeksi = stoi(komennon_osat.at(1)) - 1;
            Pankkitili& lahtotili = tilit.at(eka_indeksi);

            // Peruskomennot ovat melko suoraviivaisia.
            if (toimenpide == "tulosta") {
                lahtotili.tulosta_tiedot();

            } else if (toimenpide == "aseta") {
                aseta_tiedot(lahtotili, eka_indeksi);

            } else if (toimenpide == "pano") {
                suorita_pano(lahtotili, eka_indeksi);

            } else if (toimenpide == "otto") {
                suorita_otto(lahtotili, eka_indeksi);

            // Tilisiirto -toimenpiteeseen siirrytään vain kyseisillä ehdoilla.
            } else if (toimenpide == "tilisiirto" and komennon_osat.size() == 3) {

                // Otetaan toinen tiliolio käyttöön ja lähdetään suorittamaan toimintoa.
                int toka_indeksi = stoi(komennon_osat.at(2)) - 1;
                Pankkitili& kohdetili = tilit.at(toka_indeksi);
                suorita_tilisiirto(lahtotili, kohdetili);

            } else {

                tulosta_virheilmoitus("väärä komenoto.");
            }
        }
    }
}

