#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>

// Qt luo oletuksena alla olevat luokat, rakentajan, periytykset ja purkajan.

namespace Ui {
class Painoindeksilaskuri;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

// hh -tiedostoa on muutettu ainoastaan public slottien osalta. Ne muodostavat kuitenkin
// ohjelman toiminnallisen ytimen: metodit sisältävät kaikki toiminnallisuudet, joita
// Designerin signaaleilla ei saada aikaan. Ks. lisää metodeista mainwindow.cpp:stä.

public slots:
    void nollaa_ikkunan_arvot();
    double laske_BMI();
    void nayta_BMI_labelissa();
    void tulkitse_miehen_BMI_labelissa(const double& BMI);
    void tulkitse_naisen_BMI_labelissa(const double& BMI);
    void nayta_sukupuoli_labelissa();

private:
    Ui::Painoindeksilaskuri *ui;
};

#endif // MAINWINDOW_HH
