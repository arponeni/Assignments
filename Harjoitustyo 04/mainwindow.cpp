#include "mainwindow.hh"
#include "ui_mainwindow.h"

#include <QString>
#include <QVector>

/* Pääikkunan rakentaja: Qt luo automaattisesti rivit 13-18. Ottaa parametrinaan kanta-
 * luokan muistiosoitteen ja luo käyttöliittymän (ui).
 *
 * Mielenkiintoisempia ohjelman kannalta ovat ActionGroup ja connectionit, jotka liittävät
 * slotit toisiina halutulla tavalla.
 */

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Painoindeksilaskuri)
{
    ui->setupUi(this);

    // Varataan muistia QActionGroupille sukupuoliryhmiä varten ja luodaan actionit.
    // Actioneiden avulla voidaan käsitellä eri BMI -arvoja eri sukupuolille. Varattu
    // muisti vapautetaan purkajan avulla (ks. seuraava metodi).

    QActionGroup* sukupuoli_ryhmat = new QActionGroup(ui->menu_Sukupuoli);
    sukupuoli_ryhmat->addAction(ui->action_Mies);
    sukupuoli_ryhmat->addAction(ui->action_Nainen);

    // Lopeta -toiminnan avulla ohjelma suljetaan.

    connect(ui->action_Lopeta, SIGNAL(triggered(bool)), this, SLOT(close()));

    // Alkutila -toiminnan avulla eri widgetit ja labelit asetetaan samaan tilaan, jossa
    // ne olivat, kun ohjelma käynnistettiin. Ks. lisää 'nollaa_ikkunan_arvot'.

    connect(ui->action_Alkutila, SIGNAL(triggered(bool)), this,
            SLOT(nollaa_ikkunan_arvot()));

    // Designerissa sliderit ja spinboxit ovat linkitetty arvoiltaan toisiina. Valitsin
    // sliderit connecteihin, mutta yhtä hyvin sen olisi voinut tehdä spinboxien avulla.
    // Joka tapauksessa, kun sliderien arvot muuttuvat, lasketaan uusi BMI ja näytetään
    // vastaava selite. Ks. lisää 'nayta_BMI_labelissa'.

    connect(ui->pituus_slider, SIGNAL(valueChanged(int)), this,
            SLOT(nayta_BMI_labelissa()));
    connect(ui->paino_slider, SIGNAL(valueChanged(int)), this,
            SLOT(nayta_BMI_labelissa()));

    // BMI:n selite ei ole sama naisille ja miehille. Siksi, kun sukupuolta vaihdetaan,
    // uusi selite tulee näyttää.

    connect(ui->action_Mies, SIGNAL(triggered(bool)), this, SLOT(nayta_BMI_labelissa()));
    connect(ui->action_Nainen, SIGNAL(triggered(bool)), this, SLOT(nayta_BMI_labelissa()));

    // Näytetään labelissa, kumpi sukupuoli on valittu. Ks. lisää
    // 'nayta_sukupuoli_labelissa'.

    connect(ui->action_Mies, SIGNAL(triggered(bool)), this,
            SLOT(nayta_sukupuoli_labelissa()));
    connect(ui->action_Nainen, SIGNAL(triggered(bool)), this,
            SLOT(nayta_sukupuoli_labelissa()));
}

// Pääikkunaluokan purkaja, jossa käyttöliittymän varaama muisti poistetaan.

MainWindow::~MainWindow()
{
    delete ui;
}

// Metodin avulla nollataan pääikkunan arvot: Oletussukupuoleksi asetetaan nainen,
// sliderien (ja siten spinboxien) arvoiksi nolla ja teksteiksi viivat.

void MainWindow::nollaa_ikkunan_arvot() {

    ui->paino_slider->setValue(0);
    ui->pituus_slider->setValue(0);
    ui->action_Nainen->setChecked(true);
    ui->laskettu_BMI_label->setText("-");
    ui->tulkittu_BMI_label->setText("-");
    ui->valittu_sukupuoli_label->setText("nainen");

}

// Metodin avulla lasketaan BMI: haetaan sliderien arvot, suoritetaan lasku ja palautetaan
// double -tyyppinen BMI jatkokäsittelyyn.

double MainWindow::laske_BMI() {

    int pituus_cm = ui->pituus_slider->value();
    int paino_kg = ui->paino_slider->value();

    double pituus_m = pituus_cm / 100.0;
    double BMI_double = paino_kg / (pituus_m * pituus_m);

    return BMI_double;

}

// Metodin avulla näytetään BMI labelissa ja kutsutaan metodia, joka näyttää selitteen
// labelissa. Ei ota parametreja tai palauta mitään.

void MainWindow::nayta_BMI_labelissa() {

    // Haetaan pituus, paino ja BMI.

    int pituus = ui->pituus_slider->value();
    int paino = ui->paino_slider->value();

    // isChecked -metodin avulla tiedetään, mikä action on käynnissä.

    bool nainen_painettu = ui->action_Nainen->isChecked();

    // Jos pituus tai paino on nolla, BMI:tä on turha laskea.

    if (pituus > 0 and paino > 0) {

        double BMI = laske_BMI();

        // setNum -metodin avulla labelissa voidaan esittää double -tyyppisiä arvoja.

        ui->laskettu_BMI_label->setNum(BMI);

        // Jos nainen -action on käynnissä esitetään selite naisen mukaan.

        if (nainen_painettu) {

            tulkitse_naisen_BMI_labelissa(BMI);

        // Muussa tapauksessa mies -action on käynnissä ja tulkitaan BMI miehen mukaan.

        } else {

            tulkitse_miehen_BMI_labelissa(BMI);
        }

    // Jos pituus tai paino on nolla, laitetaan BMI:ksi ja selitteeksi viiva.

    } else {

        ui->laskettu_BMI_label->setText("-");
        ui->tulkittu_BMI_label->setText("-");
    }

}

// Metodin avulla näytetään labelissa, kumpi sukupuoli on käytössä. Rakenne on yksin-
// kertainen, kun ymmärtää edellä kerrotun isChecked -metodin. Tulee myös huomata, että
// setText tarvitsee QString -tyyppisen tiedon.

void MainWindow::nayta_sukupuoli_labelissa() {

    bool nainen_painettu = ui->action_Nainen->isChecked();
    QString sukupuoli;

    if (nainen_painettu) {

        sukupuoli = "nainen";

    } else {

        sukupuoli = "mies";
    }

    ui->valittu_sukupuoli_label->setText(sukupuoli);
}

// Metodi ottaa viiteparametrina lasketun BMI:n, ja asettaa labeliin tulkinnan sen arvosta.
// Muuten metodin rakenne on yksinkertainen. If -puun voisi korvata esim. mapin avulla,
// mutta se ei parantaisi tehokkuutta ja ymmärrettävyys kärsisi.

void MainWindow::tulkitse_miehen_BMI_labelissa(const double& BMI) {


    if (BMI <= 20.7) {

        ui->tulkittu_BMI_label->setText("alipainoa");
        return;
    }

    if (BMI <= 26.4) {

        ui->tulkittu_BMI_label->setText("normaali");
        return;
    }

    if (BMI <= 27.8) {

        ui->tulkittu_BMI_label->setText("lievää ylipainoa");
        return;
    }

    if (BMI <= 31.1) {

        ui->tulkittu_BMI_label->setText("ylipainoa");
        return;

    } else {

        ui->tulkittu_BMI_label->setText("paljon ylipainoa");
        return;
    }
}

// Toimii samalla periaatteella kuin edellinen metodi, mutta vertailuarvot ovat hieman
// erilaiset.

void MainWindow::tulkitse_naisen_BMI_labelissa(const double& BMI) {

    if (BMI <= 19.1) {

        ui->tulkittu_BMI_label->setText("alipainoa");
        return;
    }

    if (BMI <= 25.8) {

        ui->tulkittu_BMI_label->setText("normaali");
        return;
    }

    if (BMI <= 27.3) {

        ui->tulkittu_BMI_label->setText("lievää ylipainoa");
        return;
    }

    if (BMI <= 32.2) {

        ui->tulkittu_BMI_label->setText("ylipainoa");
        return;

    } else {

        ui->tulkittu_BMI_label->setText("paljon ylipainoa");
        return;
    }
}

