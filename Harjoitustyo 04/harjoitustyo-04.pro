#-------------------------------------------------
#
# Project created by QtCreator 2017-03-25T13:20:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = harjoitustyo-04
TEMPLATE = app
CONFIG += c++11
QT += core
SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.hh

FORMS    += mainwindow.ui
