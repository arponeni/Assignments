/* Ohjelmoinnin peruskurssi TIE-02200 K2017
 * Harjoitustyö 4, graafinen painoindeksilaskuri
 * Ilari Arponen, 245637, ilari.arponen@student.tut.fi
 * Ohjelma on graafinen painoindeksilaskuri, josta löytyy myös selitteet painoindeksille
 * naiselle ja miehelle.
 */

// Qt luo oletuksena alla olevan määrittelyn.

#include "mainwindow.hh"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
