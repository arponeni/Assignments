#ifndef KAYTTOLIITTYMA_HH
#define KAYTTOLIITTYMA_HH

#include "kaikki_ketjut.hh"
#include "taulu.hh"

#include <regex>

bool kayttoliittyma(Kaikki_ketjut& Resepti);
void suorita_reppu(Kaikki_ketjut& Resepti, Taulu& Reppu, string& syoterivi);
void suorita_askarreltavissa(Kaikki_ketjut& Resepti, Taulu& Reppu, string& syoterivi,
                             smatch& tulos);
void suorita_materiaalit(Kaikki_ketjut& Resepti, smatch& tulos);
void suorita_askarreltavissa_bonus(Kaikki_ketjut& Resepti, Taulu& Reppu, string& syoterivi,
                                   smatch& tulos);

#endif // KAYTTOLIITTYMA_HH
