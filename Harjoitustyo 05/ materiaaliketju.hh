#ifndef MATERIAALIKETJU_HH
#define MATERIAALIKETJU_HH

#include "taulu.hh"

#include <string>
#include <memory>

// Kahteen suuntaan linkitetty lista, joka sisältää materiaaliketjun lähtöaineesta aina
// lopputuotteisiin asti. Esim. A, E, MMM, T.

using namespace std;

class Materiaaliketju {
    public:
        Materiaaliketju();
        void push_back(const string& materiaali);
        void push_front(const string& materiaali);
        void lisaa_eka_alkio(const string& materiaali);
        void tulosta() const;
        int pituus() const;
        string ensimmainen_materiaali() const;
        string viimeinen_materiaali() const;
        string etsi_seuraaja(const string& materiaali) const;
        bool loytyyko_materiaali_ketjusta(const string& materiaali) const;

    private:
        struct Listan_alkio {
            string data;
            shared_ptr<Listan_alkio> seuraavan_osoite;
            Listan_alkio* edellisen_osoite;
        };

        shared_ptr<Listan_alkio> ensimmaisen_osoite_;
        Listan_alkio* viimeisen_osoite_;
        int alkioiden_maara_;
};

#endif // MATERIAALIKETJU_HH
