#include "kayttoliittyma.hh"

#include <regex>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

void suorita_askarreltavissa_bonus(Kaikki_ketjut& Resepti, Taulu& Reppu, string& syoterivi, smatch& tulos) {

    string apu = syoterivi;
    apu.erase(remove(apu.begin(), apu.end(), ' '), apu.end());

    if (not Resepti.loytyyko_materiaali_ketjuista(tulos.str(2))) {

        cout << "Virhe: materiaalia ei löydy reseptistä." << endl;

    } else if (tulos.str(1).size() + tulos.str(2).size() != apu.size()) {

        cout << "Virhe: syötä vain yksi materiaali." << endl;

    } else {

        if (Resepti.onko_askarreltavissa_bonus(Reppu, tulos.str(2))) {

            cout << "On askarreltavissa." << endl;

        } else {

            cout << "Ei ole askarreltavissa." << endl;
        }
    }
}

void suorita_materiaalit(Kaikki_ketjut& Resepti, smatch& tulos) {

    if (not Resepti.loytyyko_materiaali_ketjuista(tulos.str(2))) {

        cout << "Virhe: materiaalia ei löydy reseptistä." << endl;

    } else {

        Resepti.tulosta_tarvittavat(tulos.str(2));
    }
}

void suorita_askarreltavissa(Kaikki_ketjut& Resepti, Taulu& Reppu, string& syoterivi, smatch& tulos) {

    string apu = syoterivi;
    apu.erase(remove(apu.begin(), apu.end(), ' '), apu.end());

    if (not Resepti.loytyyko_materiaali_ketjuista(tulos.str(2))) {

        cout << "Virhe: materiaalia ei löydy reseptistä." << endl;

    } else if (tulos.str(1).size() + tulos.str(2).size() != apu.size()) {

        cout << "Virhe: syötä vain yksi materiaali." << endl;

    } else {

        if (Resepti.onko_askarreltavissa(Reppu, tulos.str(2))) {

            cout << "On askarreltavissa." << endl;

        } else {

            cout << "Ei ole askarreltavissa." << endl;
        }
    }
}

void suorita_reppu(Kaikki_ketjut& Resepti, Taulu& Reppu, string& syoterivi) {

    regex MATERIAALI_REGEX("(?:_*[[:alpha:]]+_*)+");
    bool onko_materaalia_reseptissa = true;
    bool syotteessa_sama_materiaali = false;

    Reppu.tyhjenna_reppu();

    sregex_iterator iter(syoterivi.begin(), syoterivi.end(), MATERIAALI_REGEX);
    sregex_iterator end;
    ++iter;

    if (iter == end) {

        cout << "Reppu tyhjennetty." << endl;

    } else {

        while ( iter != end ) {

            if (not Resepti.loytyyko_materiaali_ketjuista(iter->str())) {

                cout << "Virhe: materiaalia ei löydy reseptistä." << endl;
                onko_materaalia_reseptissa = false;
                break;

            } else if (Reppu.loytyyko_repusta(iter->str())) {

                cout << "Virhe: syötteessä sama materiaali useasti." << endl;
                syotteessa_sama_materiaali = true;
                break;
            }

            Reppu.push_back(iter->str());
            ++iter;
        }

        if (not onko_materaalia_reseptissa or syotteessa_sama_materiaali) {

            Reppu.tyhjenna_reppu();

        } else {

            cout << "Repun sisalto asetettu." << endl;
        }
    }
}

bool kayttoliittyma(Kaikki_ketjut& Resepti) {

    regex OIKEANLAINEN_KOMENTO(
                "[[:space:]]*"
                "((?:_*[[:alpha:]]+_*)+)"
                "(?:[[:space:]]+((?:_*[[:alpha:]]+_*)+)[[:space:]]*)*"
                "[[:space:]]*"
                );

    string syoterivi;
    smatch tulos;
    Taulu Reppu;

    cout << "testikayttoliittyma> ";

    while ( getline(cin, syoterivi) ) {

        if (regex_match(syoterivi, tulos, OIKEANLAINEN_KOMENTO)) {

            if (tulos.str(1) == "loppu") {

                return true;

            } else if (tulos.str(1) == "reppu") {

                suorita_reppu(Resepti, Reppu, syoterivi);

            } else if (tulos.str(1) == "tulostareppu" and tulos.str(2).empty()) {

                if (not Reppu.empty()) {

                    Reppu.print();
                }

            } else if (tulos.str(1) == "askarreltavissa") {

                suorita_askarreltavissa(Resepti, Reppu, syoterivi, tulos);

            } else if (tulos.str(1) == "materiaalit") {

                suorita_materiaalit(Resepti, tulos);
                
            } else if (tulos.str(1) == "askarreltavissa_bonus") {
                
                suorita_askarreltavissa_bonus(Resepti, Reppu, syoterivi, tulos);

            } else {

                cout << "Virhe: väärä komento" << endl;
            }

        } else {

            return false;
        }

        cout << "testikayttoliittyma> ";
    }

    return true;
}
