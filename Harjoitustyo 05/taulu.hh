#ifndef REPPU_HH
#define REPPU_HH

#include <string>

using namespace std;

// Dynaaminen taulu, jota käytetään mm. repun toteuttamisessa.

class Taulu {
    public:
        Taulu();
       ~Taulu();
        bool empty() const;
        void push_back(string materiaali);
        void tyhjenna_reppu();
        void print() const;
        bool rivi_tullut_jo(const string& lopputuote, const string& lahtoaine) const;
        bool loytyyko_repusta(const string& materiaali) const;
        void tulosta_aakkosjarjestyksessa() const;

    private:
        string* taulukko_;
        int kokonaistila_;
        int kaytetty_tila_;
};

#endif // REPPU_HH
