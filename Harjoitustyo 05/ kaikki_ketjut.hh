#ifndef KAIKKI_KETJUT_HH
#define KAIKKI_KETJUT_HH

#include "materiaaliketju.hh"
#include "taulu.hh"

#include <memory>
#include <string>

// Kahteen suuntaan linkitetty lista, johon tallennetaan kaikki materiaaliketjut.

class Kaikki_ketjut {
    public:
        Kaikki_ketjut();
        Kaikki_ketjut(const Kaikki_ketjut& alustusarvo) = delete;
        Kaikki_ketjut& operator = (const Kaikki_ketjut& sijoitusarvo) = delete;
        void tallenna_rakenteisiin(const string& lopputuote, const string& lahtoaine);
        void push_back(Materiaaliketju& ketju);
        void lisaa_eka_alkio(Materiaaliketju& ketju);
        string palauta_mahdollinen_seuraaja(const string& lahtoaine) const;
        void tulosta() const;
        int pituus() const;
        bool loytyyko_materiaali_ketjuista(const string& materiaali) const;
        bool onko_askarreltavissa(Taulu& Reppu, const string& askarreltava) const;
        void tulosta_tarvittavat(const string& materiaali) const;
        bool onko_askarreltavissa_bonus(Taulu& Reppu, const string& askarreltava) const;

    private:
        struct Listan_alkio {
            Materiaaliketju data;
            shared_ptr<Listan_alkio> seuraavan_osoite;
            Listan_alkio* edellisen_osoite;
        };

        shared_ptr<Listan_alkio> ensimmaisen_osoite_;
        Listan_alkio* viimeisen_osoite_;
        int alkioiden_maara_;
};

#endif // KAIKKI_KETJUT_HH
