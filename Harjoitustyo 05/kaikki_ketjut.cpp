#include "kaikki_ketjut.hh"

#include <iostream> 

using namespace std;


Kaikki_ketjut::Kaikki_ketjut() {

    ensimmaisen_osoite_ = nullptr;
    viimeisen_osoite_ = nullptr;
    alkioiden_maara_ = 0;
}

void Kaikki_ketjut::tallenna_rakenteisiin(const string &lopputuote, const string &lahtoaine) {

    bool luodaanko_uusi_ketju = true;
    shared_ptr<Listan_alkio> lapikaytava = ensimmaisen_osoite_;

    while (lapikaytava != nullptr) {

        if (lapikaytava->data.viimeinen_materiaali() == lopputuote/* Jos lopputuote löytyy jostain vikana */) {
             //push_back lähtöaine ja etsi sekä push_back mahdolliset seuraajat
            lapikaytava->data.push_back(lahtoaine);
            luodaanko_uusi_ketju = false;

            string seuraaja = palauta_mahdollinen_seuraaja(lahtoaine);
            if (seuraaja != "") {

                lapikaytava->data.push_back(seuraaja);
            }
        }

        if (lapikaytava->data.ensimmainen_materiaali() == lahtoaine/* Jos lähtöaine löytyy jonkin ketjun ekana */) {
            // push.front lopputuote
            lapikaytava->data.push_front(lopputuote);
            luodaanko_uusi_ketju = false;
        }

        lapikaytava = lapikaytava->seuraavan_osoite;
    }


    if (luodaanko_uusi_ketju) {

        // jos materiaalit uusia tai lopputuote löytyy jonkin ketjun ekasta tai lähtöaine ei ole minkään ekana niin uusiketju
        // uusi ketju eli uusi olio ja lisää tuotteet
        Materiaaliketju ketju;
        ketju.push_back(lahtoaine);
        ketju.push_front(lopputuote);
        push_back(ketju);
    }
}

void Kaikki_ketjut::push_back(Materiaaliketju& ketju) {

    if (pituus() == 0) {

        lisaa_eka_alkio(ketju);

    } else {

        shared_ptr<Listan_alkio> uuden_osoite(new Listan_alkio);
        uuden_osoite->data = ketju;

        uuden_osoite->seuraavan_osoite = nullptr;
        uuden_osoite->edellisen_osoite = viimeisen_osoite_;
        viimeisen_osoite_->seuraavan_osoite = uuden_osoite;
        viimeisen_osoite_ = uuden_osoite.get();
    }

    ++alkioiden_maara_;
}

void Kaikki_ketjut::lisaa_eka_alkio(Materiaaliketju &ketju) {

    shared_ptr<Listan_alkio> uuden_osoite(new Listan_alkio);
    uuden_osoite->data = ketju;

    uuden_osoite->seuraavan_osoite = nullptr;
    uuden_osoite->edellisen_osoite = nullptr;
    ensimmaisen_osoite_ = uuden_osoite;
    viimeisen_osoite_ = uuden_osoite.get();

    ++alkioiden_maara_;
}

string Kaikki_ketjut::palauta_mahdollinen_seuraaja(const string& lahtoaine) const {

    shared_ptr<Listan_alkio> lapikaytava_ketju = ensimmaisen_osoite_;

    while (lapikaytava_ketju != nullptr) {

        string seuraaja = lapikaytava_ketju->data.etsi_seuraaja(lahtoaine);

        if (seuraaja != "") {

            return seuraaja;
        }

        lapikaytava_ketju = lapikaytava_ketju->seuraavan_osoite;
    }

    return "";
}

void Kaikki_ketjut::tulosta() const {

    shared_ptr<Listan_alkio> tulostettavan_osoite = ensimmaisen_osoite_;

    while ( tulostettavan_osoite != nullptr ) {

        tulostettavan_osoite->data.tulosta();
        tulostettavan_osoite = tulostettavan_osoite->seuraavan_osoite;
    }

    cout << endl;
}

int Kaikki_ketjut::pituus() const {

    return alkioiden_maara_;

}

bool Kaikki_ketjut::loytyyko_materiaali_ketjuista(const string& materiaali) const {

    shared_ptr<Listan_alkio> lapikaytava_ketju = ensimmaisen_osoite_;

    while ( lapikaytava_ketju != nullptr ) {

        if (lapikaytava_ketju->data.loytyyko_materiaali_ketjusta(materiaali)) {

            return true;
        }

        lapikaytava_ketju = lapikaytava_ketju->seuraavan_osoite;
    }

    return false;
}

bool Kaikki_ketjut::onko_askarreltavissa(Taulu& Reppu, const string& askarreltava) const {

    shared_ptr<Listan_alkio> lapikaytava_ketju = ensimmaisen_osoite_;

    if (Reppu.loytyyko_repusta(askarreltava)) {

        return true;
    }

    while ( lapikaytava_ketju != nullptr ) {

        string tarvittava = lapikaytava_ketju->data.etsi_seuraaja(askarreltava);

        if (not Reppu.loytyyko_repusta(tarvittava) and tarvittava != "") {

            return false;
        }

        lapikaytava_ketju = lapikaytava_ketju->seuraavan_osoite;
    }

    return true;
}

void Kaikki_ketjut::tulosta_tarvittavat(const string& materiaali) const {

    shared_ptr<Listan_alkio> lapikaytava_ketju = ensimmaisen_osoite_;
    Taulu Tarvittavat;

    while ( lapikaytava_ketju != nullptr ) {

        string tarvittava = lapikaytava_ketju->data.etsi_seuraaja(materiaali);

        if (tarvittava != "" and not Tarvittavat.loytyyko_repusta(tarvittava)) {

            Tarvittavat.push_back(tarvittava);
        }

        lapikaytava_ketju = lapikaytava_ketju->seuraavan_osoite;
    }

    Tarvittavat.tulosta_aakkosjarjestyksessa();
}

bool Kaikki_ketjut::onko_askarreltavissa_bonus(Taulu& Reppu, const string& askarreltava) const {

    if (onko_askarreltavissa(Reppu, askarreltava)) {

        return true;
    }
    
    shared_ptr<Listan_alkio> lapikaytava_ketju = ensimmaisen_osoite_;

    while ( lapikaytava_ketju != nullptr ) {

        bool voidaanko_askarrella = false;

        if (lapikaytava_ketju->data.loytyyko_materiaali_ketjusta(askarreltava)) {

            string seuraaja = lapikaytava_ketju->data.etsi_seuraaja(askarreltava);

            while (seuraaja != "" ) {

                if (onko_askarreltavissa(Reppu, seuraaja)) {

                    voidaanko_askarrella = true;
                    break;
                }

                seuraaja = lapikaytava_ketju->data.etsi_seuraaja(seuraaja);
            }

            if (not voidaanko_askarrella) {

                return false;
            }
        }

        lapikaytava_ketju = lapikaytava_ketju->seuraavan_osoite;
    }

    return true;
  
}
