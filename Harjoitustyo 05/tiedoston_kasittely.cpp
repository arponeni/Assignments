* Ohjelmoinnin peruskurssi TIE-02200 K2017
 * Harjoitustyö 5, askarteluohjelma
 * Ilari Arponen, 245637, ilari.arponen@student.tut.fi
 * Ohjelma lukee lähdetiedostosta materiaalien välisiä suhteita ja tallettaa reseptin
 * dynaamiseen tietorakenteeseen. Ohjelma osaa tehdä eri operaatioita ja vertailla 
 * materiaalien välisiä suhteita.
 */

#include "tiedoston_kasittely.hh"
#include "kaikki_ketjut.hh"
#include "materiaaliketju.hh"
#include "taulu.hh"

#include <iostream>
#include <fstream>
#include <string>
#include <regex>

using namespace std;

/* Funktio lukee lähdetiedoston ja tallentaa tiedot Resepti -olioon. Itse tallennuksen
 * toteutus on kätketty olion metodeihin.
 * 
 * Parametri: Kaikki_ketjut& Resepti, dynaaminen lista, johon materiaaliketjut talletetaan.
 * 
 * Paluuarvo: true/false sen mukaan, onnistuiko tiedon luku tai tallentaminen.
 */

bool lue_tiedosto(Kaikki_ketjut& Resepti) {

    // Syotetaulu pitää kirjaa materiaaleista, jotka ovat tulleet vastaan tiedostosta.
    
    Taulu Syotetaulu;

    // Säännöllisten ilmausten avulla tarkistetaan lähdetiedoston rivien oikeellisuus.
    
    regex TYHJA_RIVI("[[:space:]]*"); // Lienee selvä.
    
    // Kommenttirivin rakenne:
    
    regex KOMMENTTI_RIVI("[[:space:]]*" // Alussa nolla tai enemmän välilyöntejä.
                         "#+"           // Pitää olla väh. yksi # -merkki.
                         ".*");         // Lopussa saa olla mitä tahansa merkkejä.
    
    // Oikeanlaisen rivin rakenne:
    
    regex OIKEANLAINEN_RIVI(
        "[[:space:]]*" 
                
        // Alla on materiaalin oikea ilmaus.Materiaali voi sisältää _ -merkkejä, kirjaimia 
        // pitää olla väh. yksi, ja lopussa voi olla _ -merkkejä. Tämä kombinaatio pitää
        // esiintyä ainakin kerran.
                
        "((?:_*[[:alpha:]]+_*)+)" 
        
        // Loput ilmaukset ovat melko selkeitä. : - merkki erottaa materiaalin, jonka
        // avulla ensiksi mainittu materiaali voidaan rakentaa.
              
        "[[:space:]]*" 
        ":" 
        "[[:space:]]*" 
        "((?:_*[[:alpha:]]+_*)+)"
        "[[:space:]]*"
        );

    string tiedoston_nimi;
    cout << "Syotetiedosto: ";
    getline(cin, tiedoston_nimi);
    
    // Mikäli tiedoston hakeminen epäonnistuu, palautetaan false.
    
    ifstream file(tiedoston_nimi);
    if ( not file ) {
        return false;
    }

    smatch tulos;
    string rivi;

    // Lähdetään lukemaan lähdetiedostoa rivi kerrallaan.
    
    while ( getline(file, rivi) ) {
        
        // Jos vastaan tulee tyhjä rivi, ei tehdä mitään.
        
        if ( regex_match(rivi, TYHJA_RIVI) ) {
            continue;
        
        // Sama homma kommenttirivin kanssa.
            
        } else if ( regex_match(rivi, KOMMENTTI_RIVI) ) {
            continue;

        // Jos rivi oli oikeaa tyyppiä, otetaan lopputuote ja lähtöaine talteen.
            
        } else if ( regex_match(rivi, tulos, OIKEANLAINEN_RIVI) ) {
            string lopputuote = tulos.str(1);
            string lahtoaine = tulos.str(2);
            
            // Mikäli rivi on tullut vastaan, palautetaan false.
            
            if (Syotetaulu.rivi_tullut_jo(lopputuote, lahtoaine)) {

                return false;
            }
            
            // Muussa tapauksessa lisätään aineet luettujen rivien tauluun ja tallennetaan
            // aineet Reseptiin.
            
            Syotetaulu.push_back(lahtoaine);
            Syotetaulu.push_back(lopputuote);
            Resepti.tallenna_rakenteisiin(lopputuote, lahtoaine);
        
        // Jos tiedoston luvussa tapahtuu jotain muuta kummaa, palautetaan false.
            
        } else {

            return false;
        }
    }
    
    // Suljetaan lopuksi tiedosta ja palautetaan true.    
   
    file.close();

    return true;
}

