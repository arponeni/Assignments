/* Ohjelmoinnin peruskurssi TIE-02200 K2017
 * Harjoitustyö 5, askarteluohjelma
 * Ilari Arponen, 245637, ilari.arponen@student.tut.fi
 * Ohjelma lukee lähdetiedostosta materiaalien välisiä suhteita ja tallettaa reseptin
 * dynaamiseen tietorakenteeseen. Ohjelma osaa tehdä eri operaatioita ja vertailla 
 * materiaalien välisiä suhteita.
 */

#include "materiaaliketju.hh"

#include <string>
#include <iostream> // muista poistaa

using namespace std;

// Luokan rakentaja: asettaa jäsenmuuttujat oikeisiin alkuarvoihin.

Materiaaliketju::Materiaaliketju() {

    ensimmaisen_osoite_ = nullptr;
    viimeisen_osoite_ = nullptr;
    alkioiden_maara_ = 0;
}

// Metodi lisää materiaaliketjun perälle materiaalin. Toiminta samanlainen kuin
// kaikki_ketjut.cpp: push_back -metodin.
 
void Materiaaliketju::push_back(const string& materiaali) {

    if (pituus() == 0) {

        lisaa_eka_alkio(materiaali);

    } else {

        shared_ptr<Listan_alkio> uuden_osoite(new Listan_alkio);
        uuden_osoite->data = materiaali;

        uuden_osoite->seuraavan_osoite = nullptr;
        uuden_osoite->edellisen_osoite = viimeisen_osoite_;
        viimeisen_osoite_->seuraavan_osoite = uuden_osoite;
        viimeisen_osoite_ = uuden_osoite.get();
    }

    ++alkioiden_maara_;
}

// Metodi lisää materiaaliketjun eteen uuden alkion. 

void Materiaaliketju::push_front(const string& materiaali) {
    
    // Mikäli kyse ensimmäisestä lisättävästä, täytyy kikkailla hieman eri tavalla.
    
    if (pituus() == 0) {

        lisaa_eka_alkio(materiaali);

    } else {
        
        // Varataan muistia uudelle alkiolle ja asetetaan kuormaksi materiaali.
        
        shared_ptr<Listan_alkio> uuden_osoite(new Listan_alkio);
        uuden_osoite->data = materiaali;
        
        // Järjestellään osoittimia halutunlaisiksi. Ks. luentomonisteen kahteen suuntaan
        // linkitetyn listan esimerkki, mikäli alla oleva näyttää kummalta.
        
        uuden_osoite->seuraavan_osoite = ensimmaisen_osoite_;
        uuden_osoite->edellisen_osoite = nullptr;
        ensimmaisen_osoite_->edellisen_osoite = uuden_osoite.get();
        ensimmaisen_osoite_ = uuden_osoite;
    }

    ++alkioiden_maara_;
}

// Metodi toimii samalla periaatteella kuin kaikki_ketjut.cpp:n vastaava.

void Materiaaliketju::lisaa_eka_alkio(const string& materiaali) {

    shared_ptr<Listan_alkio> uuden_osoite(new Listan_alkio);
    uuden_osoite->data = materiaali;

    uuden_osoite->seuraavan_osoite = nullptr;
    uuden_osoite->edellisen_osoite = nullptr;
    ensimmaisen_osoite_ = uuden_osoite;
    viimeisen_osoite_ = uuden_osoite.get();

    ++alkioiden_maara_;
}

// Tätäkään metodia ei tarvita eikä siihen pääse käsiksi käyttöliittymän kautta.
// Käytin sitä koodin testaamiseen, ja ajattelin jättää jos tulevaisuudessa tulee tarve.

void Materiaaliketju::tulosta() const {

    shared_ptr<Listan_alkio> tulostettavan_osoite = ensimmaisen_osoite_;

    while ( tulostettavan_osoite != nullptr ) {
        cout << tulostettavan_osoite->data << " ";
        tulostettavan_osoite = tulostettavan_osoite->seuraavan_osoite;
    }

    cout << endl;
}

// Metodin toiminta ja tarkoitus lienee selvä.

int Materiaaliketju::pituus() const {

    return alkioiden_maara_;
}

// Palauttaa siis materiaaliketjun ensimmäisen materiaalin. Mikäli materiaaleja ei ole,
// palautetaan merkkijono "".

string Materiaaliketju::ensimmainen_materiaali() const {

    if (pituus() > 0) {

        return ensimmaisen_osoite_->data;

    } else {

        return "";
    }

}

// Metodin toiminta ja tarkoitus lienee selvä.

string Materiaaliketju::viimeinen_materiaali() const {

    if (pituus() > 0) {

        return viimeisen_osoite_->data;

    } else {

        return "";
    }

}

/* Metodi etsii ketjusta tietyn materiaalin seuraajan, eli aineen, jonka avulla parametrin
 * voi askarrella.
 * 
 * Parametri: const string& materiaali: aine, jonka seuraaja halutaan selville.
 * 
 * Paluuarvo: merkkijono: jos seuraajaa ei löydy, palautetaan "".
 */

string Materiaaliketju::etsi_seuraaja(const string& materiaali) const {
    
    // Käydään ketju materiaali kerrallaan läpi.
    
    shared_ptr<Listan_alkio> lapikaytava = ensimmaisen_osoite_;

    while (lapikaytava != nullptr) {
        
        // Mikäli materiaali löytyy ketjusta, ja sillä on olemassa seuraaja (seuraavan
        // osoite ei ole nullptr), niin palautetaan se.
        
        if (lapikaytava->data == materiaali and 
            lapikaytava->seuraavan_osoite != nullptr) {

            return lapikaytava->seuraavan_osoite->data;
        }

        lapikaytava = lapikaytava->seuraavan_osoite;
    }
    
    // Mikäli tänne päästiin, seuraajaa ei löytynyt ja palautetaan "".
    
    return "";
}

/* Metodi tutkii, löytyykö materiaali ketjusta.
 * 
 * Parametri: const string& materiaali: aine, jonka olemassaolo halutaan selvittää.
 * 
 * Paluuarvo: true/false sen mukaan löytyikö vai ei.
 */ 

bool Materiaaliketju::loytyyko_materiaali_ketjusta(const string& materiaali) const {
    
    // Käydään siis ketju läpi, ja jos materiaali löytyy niin palautetaan true.
    
    shared_ptr<Listan_alkio> lapikaytava = ensimmaisen_osoite_;

    while (lapikaytava != nullptr) {

        if (lapikaytava->data == materiaali) {

            return true;
        }

        lapikaytava = lapikaytava->seuraavan_osoite;
    }
    
    // Jos tänne asti päästiin, materiaalia ei löytynyt, joten palautetaan false.
    
    return false;
}
