/* Ohjelmoinnin peruskurssi TIE-02200 K2017
 * Harjoitustyö 5, askarteluohjelma
 * Ilari Arponen, 245637, ilari.arponen@student.tut.fi
 * Ohjelma lukee lähdetiedostosta materiaalien välisiä suhteita ja tallettaa reseptin
 * dynaamiseen tietorakenteeseen. Ohjelma osaa tehdä eri operaatioita ja vertailla 
 * materiaalien välisiä suhteita.
 */

#include "tiedoston_kasittely.hh"
#include "kaikki_ketjut.hh"
#include "materiaaliketju.hh"
#include "kayttoliittyma.hh"

#include <iostream>
#include <string>

using namespace std;

/* Lienee tässä kohtaa hyvä selventää toteutustani. Ratkaisuni tiedon tallentamiseen oli,
 * että luon jokaista materiaaliketjua kohden, esim. A -> E -> MMM -> T, olion, jotka
 * sitten talletan reseptiin. Voisikin siis ajatella, että resepti on lista, jonka sisällä
 * lista. Tehtävänannosta rohkaistiin käyttämään shared pointereita, joten tämä resepti
 * rakentuu niiden varaan. Repun toteutin taulukon avulla, mikä olikin hyvä ratkaisu.
 * 
 * Toivottavasti koodi on ymmärrettävää, hyviä lukuhetkia!
 */



int main() {
    
    // Luodaan Resepti -olio, johon tallennetaan materiaaliketjut. Jos tiedon lukeminen
    // epäonnistuu, ohjelma loppuu.

    Kaikki_ketjut Resepti;

    if (not lue_tiedosto(Resepti)) {

        cout << "Virhe: tiedoston lukeminen epäonnistui." << endl;
        return 1;
    }
    
    // Kun tiedot ovat luettu onnistuneesti, siirrytään käyttöliittymään. Jos jokin menee
    // pieleen, palautetaan mainista arvo 1. Jos taas kaikki onnistuu, palautetaan 0.
    
    cout << "Tuotantoketjut luettu onnistuneesti." << endl;
    
    if (not kayttoliittyma(Resepti)) {

        cout << "Virhe: jotain odottamatonta tapahtui." << endl;
        return 1;
    }

    return 0;
}
