#ifndef TIEDOSTONLUKU_HH
#define TIEDOSTONLUKU_HH

#include "kaikki_ketjut.hh"

#include <string>

bool lue_tiedosto(Kaikki_ketjut& resepti);

#endif // TIEDOSTONLUKU_HH
