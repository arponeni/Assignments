/* Ohjelmoinnin peruskurssi TIE-02200 K2017
 * Harjoitustyö 5, askarteluohjelma
 * Ilari Arponen, 245637, ilari.arponen@student.tut.fi
 * Ohjelma lukee lähdetiedostosta materiaalien välisiä suhteita ja tallettaa reseptin
 * dynaamiseen tietorakenteeseen. Ohjelma osaa tehdä eri operaatioita ja vertailla 
 * materiaalien välisiä suhteita.
 */

#include "taulu.hh"

#include <string>
#include <iostream>

using namespace std;

// Kyseisen koodin monet toiminnot ovat käytännössä samat kuin ennakkotehtävässä 11, 
// minkä vuoksi jätän kommentoinnin melko vähälle.

// Taulun alkukoko olkoon 5.

const unsigned int ALKUKOKO = 5;

// Luokan rakentaja. Varaa alkukoon verran muistia sekä alustaa jäsenmuuttujat.

Taulu::Taulu() {

    taulukko_ = new string[ALKUKOKO];
    kokonaistila_ = ALKUKOKO;
    kaytetty_tila_ = 0;
}

// Luokan purkaja. Vapauttaa varatun muistin ja asettaa jäsenmuuttujat haluttuun muotoon.

Taulu::~Taulu() {

    delete [] taulukko_;
    kaytetty_tila_ = 0;
    kokonaistila_ = 0;
    taulukko_ = nullptr;
}

// Metodi tutkii onko taulu tyhjä vai ei.

bool Taulu::empty() const {

    return kaytetty_tila_ == 0;
}

// Metodi lisää tauluun uuden alkion.

void Taulu::push_back(string materiaali) {

    if (kokonaistila_ > kaytetty_tila_) {

        taulukko_[kaytetty_tila_] = materiaali;
        ++kaytetty_tila_;

    // Taulukko liian pieni pitää varata uusi isompi.

    } else {

        string* uusi_taulukko;
        int  uusi_tilan_maara;

        // Uusi taulukko_ on kooltaan kaksi
        // kertaa suurempi kuin vanha.
        
        uusi_tilan_maara = 2 * kokonaistila_;
        uusi_taulukko = new string[uusi_tilan_maara];

        // Kopioidaan uuteen taulukko_on kaikki
        // alkiot vanhasta taulukosta.
        
        for (int i = 0; i < kaytetty_tila_; ++i) {
            uusi_taulukko[i] = taulukko_[i];
        }

        // Vapautetaan vanha taulukko_.
        
        delete[] taulukko_;

        // Päivitetään jäsenmuuttujat uuden
        // taulukon mukaisiksi.
        
        taulukko_ = uusi_taulukko;
        kokonaistila_ = uusi_tilan_maara;

        // Nokkela rekursio: vältetään kirjoittamasta
        // if-rakenteen tosihaaran runkoa uudelleen.
        // Rekursiivinen lisäys onnistuu varmasti ilman
        // muistin varausta, koska se on juuri tehty edellä.
        
        push_back(materiaali);
    }
}

// Metodi tyhjentää taulun. Toimii melko samoin kuin luokan purkaja.

void Taulu::tyhjenna_reppu() {

    delete[] taulukko_;

    taulukko_ = new string[ALKUKOKO];
    kaytetty_tila_ = 0;
    kokonaistila_ = ALKUKOKO;
}

// Metodi tulostaa taulun alkiot.

void Taulu::print() const {

    int indeksi = 0;

    while ( indeksi < kaytetty_tila_ ) {

        cout << taulukko_[indeksi] << endl;
        ++indeksi;
    }
}

/* Metodi tutkii tiedoston käsittelyn yhteydessä, onko rivi tullut jo vastaan.
 * 
 * Parametrit: const string& lopputuote: aine, joka voidaan askarrella lähtöaineesta.
 *             const string& lahtoaine: lienee selvää.
 * 
 * Paluuarvo: true/false sen mukaan, onko rivi tullut vastaan.
 */

bool Taulu::rivi_tullut_jo(const string& lopputuote, const string& lahtoaine) const {
    
    // Käydään taulukko läpi ja verrataan alkioita parametreihin. Jos tulee vastaan niin
    // palautetaan true.
    
    for (int i = 0; i < kaytetty_tila_ - 1; i=i+2) {

        if (taulukko_[i] == lahtoaine and taulukko_[i+1] == lopputuote) {

            return true;
        }
    }

    return false;
}

// Metodi tutkii, löytyykö tietty materiaali taulukosta.

bool Taulu::loytyyko_repusta(const string& materiaali) const {

    for (int i = 0; i < kaytetty_tila_; ++i) {

        if (taulukko_[i] == materiaali) {

            return true;
        }
    }

    return false;
}

/* Seuraava koodi on osittain saatu osoitteesta: 
 * https://mathbits.com/MathBits/CompSci/Arrays/Bubble.htm
 * Kyseessä on nk. bubble-switch, jonka järjestäminen perustuu siihen, että käydään
 * silmukassa läpi taulukkoa, ja vertaillaan aina kahden peräkkäisen alkion suuruutta.
 * Peräkkäisten alkioiden järjestystä vaihdetaan niin kauan, kunnes koko taulukko on
 * järjestyksessä.
 */ 

void Taulu::tulosta_aakkosjarjestyksessa() const {
    
    // Jos taulukko on tyhjä, ei tehdä mitään, vaan palataan.
    
    if (empty()) {

        return;
    }
    
    // Apumuuttuja flag tarkistaa, onko taulukko oikein järjestetty. 
    
    int i, j, flag = 1;
    string min;                     // Tähän tallennetaan hetkellinen minimi.
    int numLength = kaytetty_tila_; // Taulun koko.

    for (i = 1; (i <= numLength) and flag; i++) {

        flag = 0; // Merkitsee, että alkioiden paikka ei ole vielä vaihtunut.

        for (j=0; j < (numLength -1); j++) {

            // Mikäli seuraava alkio on pienempi kuin edellinen, vaihdetaan alkioiden
            // paikat.

            if (taulukko_[j+1] < taulukko_[j]) {

                min = taulukko_[j];
                taulukko_[j] = taulukko_[j+1];
                taulukko_[j+1] = min;
                flag = 1;               // Tarkoittaa, että vaihto tapahtui.
           }
        }
    }

    print();
}
